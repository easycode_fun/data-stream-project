# data-stream-project

[data-stream](https://xuzhen97.notion.site/data-stream-cc4e57af3dd541fc988a4c1eae0800d1)

业务处理流程
![img.png](docs/img.png)

整体的架构图
![img1.png](docs/img1.png)

增量处理模型
![img2.png](docs/img2.png)

全量处理模型
![img3.png](docs/img3.png)