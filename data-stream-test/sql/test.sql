CREATE DATABASE `demo1` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

CREATE TABLE demo1.test (
                            id varchar(20) NOT NULL,
                            name varchar(100) NULL,
                            CONSTRAINT test_PK PRIMARY KEY (id)
)
    ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_0900_ai_ci
COMMENT='test';

INSERT INTO demo1.test
(id, name)
VALUES('1', 'hello');


CREATE DATABASE `demo2` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

CREATE TABLE demo2.test1 (
                             id varchar(20) NOT NULL,
                             name varchar(100) NULL,
                             CONSTRAINT test1_PK PRIMARY KEY (id)
)
    ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_0900_ai_ci
COMMENT='test1';
