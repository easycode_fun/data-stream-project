package fun.easycode.datastream.service.report_test;

import fun.easycode.datastream.IDataProcessor;
import fun.easycode.datastream.dao.demo1.Test;
import fun.easycode.datastream.dao.demo1.TestMapper;
import fun.easycode.jointblock.util.JacksonUtil;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author xuzhen97
 */
@Component
public class TestProcessor implements IDataProcessor<Test> {

    @Override
    public void process(List<Test> input) {
        System.err.println(JacksonUtil.toJson(input));
        System.err.println("ReportStoreInventoryProcessor.process");
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.err.println("ReportStoreInventoryProcessor.process end");
    }

    @Override
    public void cleanUp(List<Test> cleanUpInput) {
        System.err.println(JacksonUtil.toJson(cleanUpInput));
        System.err.println("ReportStoreInventoryProcessor.cleanUp");
    }

    @Override
    public DataProcessorParam<Test> getParam() {

        DataProcessorParam<Test> param = new DataProcessorParam<>();
        param.setInputClass(Test.class);
        param.setInputMapper(TestMapper.class);
        param.setOutputTableName("report_test");
        return param;
    }
}
