package fun.easycode.datastream.service.report_test1;

import fun.easycode.datastream.IDataProcessor;
import fun.easycode.datastream.dao.demo2.Test1;
import fun.easycode.datastream.dao.demo2.Test1Mapper;
import fun.easycode.jointblock.util.JacksonUtil;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Test1Processor implements IDataProcessor<Test1> {
    @Override
    public void process(List<Test1> input) {
        System.err.println(JacksonUtil.toJson(input));
        System.err.println("ReportStoreInventoryProcessor.process");
//        throw new RuntimeException("test1");
    }

    @Override
    public void cleanUp(List<Test1> cleanUpInput) {
        System.err.println(JacksonUtil.toJson(cleanUpInput));
        System.err.println("ReportStoreInventoryProcessor.cleanUp");
    }

    @Override
    public DataProcessorParam<Test1> getParam() {
        DataProcessorParam<Test1> param = new IDataProcessor.DataProcessorParam<>();
        param.setOutputTableName("report_test1");
        param.setInputClass(Test1.class);
        param.setInputMapper(Test1Mapper.class);
        return param;
    }
}
