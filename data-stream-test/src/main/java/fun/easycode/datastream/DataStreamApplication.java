package fun.easycode.datastream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 * @author xuzhen97
 */
@SpringBootApplication
public class DataStreamApplication {
    public static void main(String[] args) {
        SpringApplication.run(DataStreamApplication.class, args)    ;
    }
}
