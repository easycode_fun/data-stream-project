package fun.easycode.datastream.dao.demo2;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;

/**
* <p>
*  Repository
* </p>
*
* @author xuzhen97
* @since 2023-04-21
*/
@Repository
public class Test1Repository extends ServiceImpl<Test1Mapper, Test1> {

}
