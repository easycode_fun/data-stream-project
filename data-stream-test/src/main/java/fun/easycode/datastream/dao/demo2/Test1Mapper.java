package fun.easycode.datastream.dao.demo2;

import fun.easycode.jointblock.core.JointBlockMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xuzhen97
 * @since 2023-04-21
 */
public interface Test1Mapper extends JointBlockMapper<Test1> {

}
