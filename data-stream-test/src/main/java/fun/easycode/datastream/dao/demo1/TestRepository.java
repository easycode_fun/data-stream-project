package fun.easycode.datastream.dao.demo1;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;

/**
* <p>
* 测试表 Repository
* </p>
*
* @author xuzhen97
* @since 2023-04-21
*/
@Repository
public class TestRepository extends ServiceImpl<TestMapper, Test> {

}
