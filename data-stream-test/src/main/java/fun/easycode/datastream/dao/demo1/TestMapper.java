package fun.easycode.datastream.dao.demo1;

import fun.easycode.jointblock.core.JointBlockMapper;

/**
 * <p>
 * 测试表 Mapper 接口
 * </p>
 *
 * @author xuzhen97
 * @since 2023-04-21
 */
public interface TestMapper extends JointBlockMapper<Test> {

}
