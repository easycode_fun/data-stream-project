package fun.easycode.datastream.repository;

import fun.easycode.jointblock.core.Enumerator;

/**
 * 任务执行状态枚举
 * @author xuzhen97
 */
public enum TaskState implements Enumerator<Integer> {
    /**
     * 未开始
     */
    NOT_START(1, "未开始"),
    /**
     * 等待中，代表任务已经发送到mq中，等待消费执行
     */
    WAIT(2, "等待中"),
    /**
     * 执行中, 消息已经被消费，正在执行
     */
    EXECUTING(3, "执行中"),
    /**
     * 执行成功
     */
    SUCCESS(4,"执行成功"),
    /**
     * 执行失败
     */
    EXEC_FAILURE(5, "执行失败")
    ;

    private final Integer value;
    private final String desc;

    TaskState(Integer value, String desc){
        this.value = value;
        this.desc = desc;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }
}
