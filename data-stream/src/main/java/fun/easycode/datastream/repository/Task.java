package fun.easycode.datastream.repository;

import cn.hutool.core.net.NetUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 数据任务
 * </p>
 *
 * @author xuzhen97
 * @since 2023-05-04
 */
@Getter
@Setter
@TableName("task")
public class Task implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 环境
     */
    @TableField("environment")
    private String environment;

    /**
     * 实例ip
     */
    @TableField("instance_ip")
    private String instanceIp;

    /**
     * 年
     */
    @TableField("year")
    private Integer year;

    /**
     * 月
     */
    @TableField("month")
    private Integer month;

    /**
     * 日
     */
    @TableField("day")
    private Integer day;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 最后修改时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 执行时间，存储秒s
     */
    @TableField("execution_time")
    private Integer executionTime;

    /**
     * 参数，全量存储sql片段
     */
    @TableField("param")
    private String param;

    /**
     * 执行状态
     */
    @TableField("state")
    private TaskState state;

    /**
     * 处理器名称
     */
    @TableField("processor_name")
    private String processorName;

    /**
     * 最后一次失败原因
     */
    @TableField("last_error_msg")
    private String lastErrorMsg;

    /**
     * 总条数
     */
    @TableField("total_size")
    private Integer totalSize;

    /**
     * 执行开始的时间戳，用于计算用时
     */
    @TableField("exe_start_millis")
    private Long exeStartMillis;


    /**
     * 设置基础字段
     */
    public void setBaseEntity(){
        LocalDateTime now = LocalDateTime.now();
        String profile = SpringUtil.getActiveProfile();
        this.setEnvironment(profile!=null? profile : "default");
        this.setInstanceIp(NetUtil.getLocalhostStr());
        this.setYear(now.getYear());
        this.setMonth(now.getMonthValue());
        this.setDay(now.getDayOfMonth());
    }
}
