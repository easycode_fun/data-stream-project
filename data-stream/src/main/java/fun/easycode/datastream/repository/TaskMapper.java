package fun.easycode.datastream.repository;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 数据任务 Mapper 接口
 * </p>
 *
 * @author xuzhen97
 * @since 2023-05-04
 */
@Mapper
@DS("data-stream")
public interface TaskMapper extends BaseMapper<Task> {

}
