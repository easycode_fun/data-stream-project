package fun.easycode.datastream;

/**
 * canal 监听器
 * @author xuzhen97
 * @param <T> 数据类型
 */
public interface CanalListener<T> {
    /**
     * 监听数据
     * @param entry 数据
     */
    void onMessage(DataEntry<T> entry);

    /**
     * 监听表名
     * @return 表名
     */
    String getTableName();

    /**
     * 监听数据类型
     * @return 数据类型
     */
    Class<T> getTargetClass();

    /**
     * 监听数据标识
     * @return 数据标识
     */
    String getMark();
}
