package fun.easycode.datastream;

import fun.easycode.jointblock.validator.IValidate;
import lombok.Data;
import org.springframework.beans.factory.InitializingBean;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.sql.Wrapper;
import java.util.List;

/**
 * 数据转换接口
 * @author xuzhen97
 * @param <T> 输入数据类型
 * @param <R> 输出数据类型
 */
public interface IDataTransformer<T,R> extends InitializingBean,IDataContract {

    /**
     * 组装转换条件
     * @param filterInput 筛选后的数据
     * @return 组装后的条件
     */
    Wrapper getWrapper(List<T> filterInput);

    /**
     * 数据转换参数
     * @return 数据转换参数
     */
    DataTransformParam<T,R> getParam();

    /**
     * 初始化参数校验
     */
    @Override
    default void afterPropertiesSet() {
        getParam().validate();
    }

    /**
     * 数据转换参数
     * @param <T> 输入数据类型
     * @param <R> 输出数据类型
     * @see IDataTransformer#getParam()
     * @author xuzhen97
     */
    @Data
    class DataTransformParam<T,R> implements IValidate {
        @NotNull
        private Class<T> inputClass;
        @NotEmpty
        private String outputTableName;
        @NotNull
        private Class<? extends IDataProcessor<R>> processor;
    }
}
