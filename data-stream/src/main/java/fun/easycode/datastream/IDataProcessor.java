package fun.easycode.datastream;

import fun.easycode.jointblock.core.JointBlockMapper;
import fun.easycode.jointblock.validator.IValidate;
import lombok.Data;
import org.springframework.beans.factory.InitializingBean;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 数据处理器接口
 *
 * @author xuzhen97
 */
public interface IDataProcessor<T> extends InitializingBean,IDataContract {

    /**
     * 数据处理
     *
     * @param input 输入数据
     */
    void process(List<T> input);

    /**
     * 数据清理
     *
     * @param cleanUpInput 清理数据
     */
    void cleanUp(List<T> cleanUpInput);


    /**
     * 获取数据处理器参数
     *  主要是输入输出相关的实体类class和mapper
     *  这些需要用于框架执行
     * @return 数据处理器参数
     */
    DataProcessorParam<T> getParam();

    /**
     * 初始化参数校验
     */
    @Override
    default void afterPropertiesSet() {
        getParam().validate();
    }

    /**
     * 数据处理器参数
     * @param <T> 输入数据类型
     * @see IDataProcessor#getParam()
     * @author xuzhen97
     */
    @Data
    class DataProcessorParam<T> implements IValidate {
        @NotNull
        private Class<T> inputClass;
        @NotNull
        private Class<? extends JointBlockMapper<T>> inputMapper;
        @NotEmpty
        private String outputTableName;
    }
}
