package fun.easycode.datastream.api;

import fun.easycode.jointblock.core.PageQry;
import fun.easycode.jointblock.core.Sort;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author xuzhen97
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class TaskPageQry extends PageQry {
    private String id;
    private String processorName;
    private Integer state;
    private String updateTimeSort = Sort.DESC.name();
}
