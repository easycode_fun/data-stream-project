package fun.easycode.datastream.api;

import fun.easycode.jointblock.validator.IValidate;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 任务启动命令
 * @author xuzhen97
 */
@Data
public class StartTaskCmd implements IValidate {
    @NotBlank
    private String taskId;
}
