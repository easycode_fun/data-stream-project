package fun.easycode.datastream.api;

import fun.easycode.datastream.DataCompleteManager;
import fun.easycode.datastream.DataContext;
import fun.easycode.datastream.IDataProcessor;
import fun.easycode.datastream.repository.Task;
import fun.easycode.jointblock.core.CheckException;
import fun.easycode.jointblock.core.PageDto;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author xuzhen97
 */
@RestController
@RequestMapping("/data-stream")
public class DataApiRestController {

    @Resource
    private DataCompleteManager dataCompleteManager;

    @PostMapping("/create-task")
    public Task createTask(@RequestBody CreateTaskCmd cmd) {
        cmd.validate();
        IDataProcessor dataProcessor = DataContext.getProcessor(cmd.getMark());
        if (dataProcessor == null) {
            throw new CheckException("未找到对应的处理器");
        }
        return dataCompleteManager.createTask(dataProcessor, cmd.getQueryDsl());
    }

    @GetMapping("/list-task")
    public PageDto<Task> taskPageQry(TaskPageQry qry){
        return dataCompleteManager.taskPageQry(qry);
    }

    @PostMapping("/start-task")
    public String startTask(@RequestBody StartTaskCmd cmd) {
        cmd.validate();
        dataCompleteManager.startTask(cmd.getTaskId());
        return "success";
    }
}
