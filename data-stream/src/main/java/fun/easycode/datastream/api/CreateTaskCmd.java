package fun.easycode.datastream.api;

import fun.easycode.jointblock.validator.IValidate;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 任务启动命令
 * @author xuzhen97
 */
@Data
public class CreateTaskCmd implements IValidate {
    @NotBlank
    private String mark;
    private String queryDsl;
}
