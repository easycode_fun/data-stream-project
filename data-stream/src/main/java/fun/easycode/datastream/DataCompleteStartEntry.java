package fun.easycode.datastream;

import lombok.Data;

/**
 * 数据全量启动消息体
 * @author xuzhen97
 */
@Data
public class DataCompleteStartEntry {
    /**
     * 任务id
     */
    private String taskId;
}
