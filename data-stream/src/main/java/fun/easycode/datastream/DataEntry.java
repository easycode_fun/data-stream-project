package fun.easycode.datastream;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 数据项，监听canal消息后，转换成自定义数据类型发送到消息中间件中
 *  发送的就是这种格式的数据
 * @author xuzhen97
 */
@Data
public class DataEntry<T> {
    private T data;
    private DataMode dataMode;
    private Map<String, String> extra = new HashMap<>();

    public DataEntry(T data, DataMode dataMode) {
        this.data = data;
        this.dataMode = dataMode;
    }

    public DataEntry(){}

    /**
     * 添加额外的数据
     * @param key 键
     * @param value 值
     */
    public void putExtra(String key, String value){
        extra.put(key, value);
    }
}
