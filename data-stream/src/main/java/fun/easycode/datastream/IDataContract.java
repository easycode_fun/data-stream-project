package fun.easycode.datastream;

/**
 * 数据处理器接口契约
 *  抽取出来的原因是为了方便后续扩展
 * @author xuzhen97
 */
public interface IDataContract {
    /**
     * 获取数据处理器标识
     *  默认为类名 . 替换成_
     * @return 数据处理器标识
     */
    default String getMark(){
        return this.getClass().getName().replace(".","_");
    }
}
