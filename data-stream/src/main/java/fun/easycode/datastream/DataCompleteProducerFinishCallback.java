package fun.easycode.datastream;

import java.util.List;

/**
 * 任务执行完成回调接口
 * @author xuzhen97
 */
@FunctionalInterface
public interface DataCompleteProducerFinishCallback {
    /**
     * 任务执行完成
     * @param totalSize 总数量
     * @param errorMsgList 错误信息列表
     */
    void callback(long totalSize, List<String> errorMsgList);
}