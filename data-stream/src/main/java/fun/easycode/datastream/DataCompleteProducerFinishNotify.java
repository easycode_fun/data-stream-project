package fun.easycode.datastream;

/**
 * 任务执行完成标记接口
 * @author xuzhen97
 */
@FunctionalInterface
public interface DataCompleteProducerFinishNotify {
    /**
     * 任务执行完成
     */
    void finish();
}
