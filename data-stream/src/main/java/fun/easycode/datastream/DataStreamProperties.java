package fun.easycode.datastream;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * canal配置
 * @author xuzhen97
 */
@Data
@ConfigurationProperties(prefix = "data-stream")
public class DataStreamProperties {

    /**
     * rocketmq配置
     */
    private RocketMq rocketMq;

    /**
     * canal配置
     */
    private Canal canal;

    /**
     * 数据源配置
     *  主要用于全量同步时，从数据库中读取数据
     *  task
     */
    private DataSource dataSource;

    /**
     * dao包路径，用于动态数据源配置
     */
    private String daoPackage;
    /**
     * dao mapper扫描路径
     */
    private String daoMapperScan;

    @Data
    public static class DataSource {
        private String url;
        private String username;
        private String password;
        private String driverClassName;
    }

    @Data
    public static class Canal {
        /**
         * 是否是集群模式, 集群下需要配置zookeeper
         * Zookeeper.address
         */
        private boolean cluster = false;

        /**
         * zookeeper地址
         *  在集群下有用
         */
        private String zookeeperAddress;
        /**
         * canal主机, 单机模式下默认127.0.0.1
         */
        private String host = "127.0.0.1";
        /**
         * canal端口, 单机模式下默认11111
         */
        private Integer port = 11111;
        /**
         * canal目的地, canal的是实例名
         * 默认example
         */
        private String destination = "example";
        /**
         * canal用户名
         */
        private String username = "canal";
        /**
         * canal密码
         */
        private String password = "canal";
        /**
         * canal订阅信息配置
         */
        private String subscribe = ".*\\..*";
    }

    /**
     * rocketmq配置
     */
    @Data
    public static class RocketMq {
        /**
         * name server地址
         */
        private String nameSrvAddr;
        /**
         * 默认topic
         */
        private String defaultTopic;
        /**
         * 默认转换topic
         */
        private String defaultTransformTopic;
        /**
         * 默认全量topic
         */
        private String defaultCompleteTopic;
    }
}
