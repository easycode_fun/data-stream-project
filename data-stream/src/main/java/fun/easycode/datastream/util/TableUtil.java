package fun.easycode.datastream.util;

import cn.hutool.core.annotation.AnnotationUtil;
import com.baomidou.mybatisplus.annotation.TableName;
import fun.easycode.jointblock.util.CamelUnderUtil;

/**
 * 数据库表工具类，封装了一些mybatis plus实体相关的操作
 * @author xuzhen97
 */
public final class TableUtil {

    /**
     * 获取表名
     * @param clazz 实体类
     * @return 表名
     */
    public static String getTableName(Class<?> clazz){
        TableName tableName = AnnotationUtil.getAnnotation(clazz, TableName.class);

        if(tableName != null){
            return tableName.value();
        }

        return CamelUnderUtil.camel2under(clazz.getSimpleName());
    }
}
