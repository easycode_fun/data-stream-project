package fun.easycode.datastream.util;

import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

/**
 * spring retry 工具类
 * 重试模板实例, 单例管理，方便框架使用
 * @author xuzhen97
 */
public class RetryUtil {
    private static final RetryTemplate RETRY_TEMPLATE;

    static {
        // 构建重试模板实例
        RETRY_TEMPLATE = new RetryTemplate();

        // 设置重试回退操作策略  设置重试间隔时间
        // 默认1000ms, 设置为2000s
        FixedBackOffPolicy backOffPolicy = new FixedBackOffPolicy();
        backOffPolicy.setBackOffPeriod(2000);

        // 设置重试策略  设置重试次数 设置异常处理结果
        // 默认时重试3次 Exception异常
        SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy();
        // 次数覆盖成10次
        retryPolicy.setMaxAttempts(10);

        //  重试模板添加重试策略 添加回退操作策略
        RETRY_TEMPLATE.setRetryPolicy(retryPolicy);
        RETRY_TEMPLATE.setBackOffPolicy(backOffPolicy);
    }

    /**
     * 获取重试模板实例
     * @return RetryTemplate
     */
    public static RetryTemplate getRetryTemplate() {
        return RETRY_TEMPLATE;
    }
}
