package fun.easycode.datastream.util;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import java.util.Map;

public final class MyBatisPlusUtil {

    public static <T> String getCustomSqlSegment(QueryWrapper<T> wrapper) {

        String sqlSegment = wrapper.getCustomSqlSegment();

        Map<String, Object> paramNameValuePairs = wrapper.getParamNameValuePairs();

        for (Map.Entry<String, Object> entry : paramNameValuePairs.entrySet()) {
            String repKey = "#{ew.paramNameValuePairs." + entry.getKey() + "}";
            if(entry.getValue() instanceof String){
                sqlSegment = sqlSegment.replace(repKey, "'"+ entry.getValue() +"'");
            }else{
                sqlSegment = sqlSegment.replace(repKey, String.valueOf(entry.getValue()));
            }
        }
        return sqlSegment;
    }

    public static <T> String getCustomSqlSegment(LambdaQueryWrapper<T> wrapper) {
        String sqlSegment = wrapper.getCustomSqlSegment();

        Map<String, Object> paramNameValuePairs = wrapper.getParamNameValuePairs();

        for (Map.Entry<String, Object> entry : paramNameValuePairs.entrySet()) {
            String repKey = "#{ew.paramNameValuePairs." + entry.getKey() + "}";
            if(entry.getValue() instanceof String){
                sqlSegment = sqlSegment.replace(repKey, "'"+ entry.getValue() +"'");
            }else{
                sqlSegment = sqlSegment.replace(repKey, String.valueOf(entry.getValue()));
            }
        }
        return sqlSegment;
    }
}
