package fun.easycode.datastream;

/**
 * 数据处理方式
 * @author xuzhen97
 */
public enum DataMode {
    /**
     * 需要处理
     */
    COPE,
    /**
     * 需要清除
     */
    CLEAN_UP
}