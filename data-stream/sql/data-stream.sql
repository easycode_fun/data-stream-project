create table task
(
    id               varchar(20)                        not null comment '主键'
        primary key,
    environment      varchar(50)                        not null comment '环境',
    instance_ip      varchar(50)                        not null comment '实例ip',
    year             int                                not null comment '年',
    month            int                                not null comment '月',
    day              int                                null comment '日',
    create_time      datetime default CURRENT_TIMESTAMP not null comment '创建时间',
    update_time      datetime default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP comment '最后修改时间',
    execution_time   int      default 0                 not null comment '执行时间，存储秒s',
    param            text                               null comment '参数，全量存储sql片段',
    state            int                                not null comment '执行状态',
    processor_name   varchar(500)                       not null comment '处理器名称',
    last_error_msg   text                               null comment '最后一次失败原因',
    total_size       int      default 0                 not null comment '总条数',
    exe_start_millis mediumtext                         null comment '执行开始的时间戳，用于计算用时'
)
    comment '数据任务';

create index task_state_index
    on task (state);

create index task_year_month_day_index
    on task (year, month, day);